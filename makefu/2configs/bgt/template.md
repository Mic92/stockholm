# <SENDUNGSNUMMER>

0.  Sendung twittern und mastodieren (eine Woche + eine Stunde vorher) (wichtig)
1.  `eine` Person anrufen (den Host):
   - markus      162dcbf89f@studio.link
   - Felix1      makefu@studio.link
   - L33tFelix   l33tname@studio.link
   - Ingo        ingo@studio.link
2.  Jitis an machen! https://meet.jit.cloud/bgt (mittel)
3.  studio-link aufnehmen drücken, schauen ob file local.flac größer wird (wichtig)
4.  audiocity starten, 48000Hz einstellen, Audio-Device checken und aufnehmen drücken (wichtig)
4.  alternative parecord:
     `$ pacmd list-sources | grep -e device.string -e 'name:' # keins der "monitor" devices`
     `$ parecord --channels=1 -d alsa_input.usb-Burr-Brown_from_TI_USB_Audio_CODEC-00.analog-stereo bgt.wav`
5.  darkice starten (wichtig)
5.  chapter-marker starten (wichtig)
7.  klatschen
8.  Hallihallo und Herzlich Willkommen
9.  ctrl-u auf "H" von "Halli" Felix auf jeden fall erinnern (wichtig)
10. Ctrl-j drücken für neuen Eintrag - ggf. Felix erinnern (wichtig)

## Vorschläge
### Backlog von Picks und Lesefoo aus der letzten Woche

die Nachfolgenden 3 Striche sind sehr wichtig, bitte nicht löschen. Nachdem
chapter-marker gestartet wurde kann die reihenfolge nicht mer angepasst werden,
das ist der preis, den man zahlt

---

## Blast from the Past

## Toter der Woche

## Untoter der Woche

## News

## Themen

## Mimimi der Woche

## Lesefoo

## Picks

## Ende
